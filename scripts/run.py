import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import traceback
import sys
import logging
import os
import input
from pyvirtualdisplay import Display

class TestActionable(unittest.TestCase):

	def customWait(chromedriver):
		
		element = wait.until(EC.presence_of_element_located(By.NAME("password")))
		return element

	def write_file(self,data):
		file_name = input.reportFile
		with open(file_name, 'ab') as x_file:
			x_file.write('{} \n'.format(data))


	def send_email(self,user, pwd, recipient, subject, body):
		import smtplib

		gmail_user = user
		gmail_username = input.senderUsername
		gmail_pwd = pwd
		FROM = user
		TO = recipient if type(recipient) is list else [recipient]
		SUBJECT = subject
		TEXT = body

		# Prepare actual message
		message = """From: %s\nTo: %s\nSubject: %s\n\n%s
		""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
		try:
			server = smtplib.SMTP("smtp.gmail.com", 587)
			server.ehlo()
			server.starttls()
			server.login(gmail_user, gmail_pwd)
			server.sendmail(FROM, TO, message)
			server.close()
			print 'successfully sent the mail'
		except:
			traceback.print_exc()
			print "failed to send mail"

	def setUp(self):
		if(input.env is "server"):
			display = Display(visible=0, size=(800, 600))
			display.start()
		chromeOption = webdriver.ChromeOptions()
		chromeOption.add_extension(input.appPath)
		self.driver = webdriver.Chrome(input.driverPath,chrome_options = chromeOption)
		self.driver.get('https://gmail.com')

	def testActionable(self):
		driver = self.driver
		mailWindow = driver.window_handles[0]
		username = driver.find_element_by_id("identifierId")
		username.send_keys(input.email)
		nextButton = driver.find_element_by_xpath("//span[text()='Next']")
		nextButton.click()
		time.sleep(2)
		wait = WebDriverWait(driver,10)
		element = wait.until(EC.presence_of_element_located((By.NAME,"password")))
		if  element is None:
			return
		else:
			passwordField = driver.find_element_by_name("password")
			passwordField.send_keys(input.password)
		time.sleep(3)
		nextButton2 = driver.find_element_by_xpath("//span[text()='Next']")
		nextButton2.click()
		time.sleep(10)
		mailWindow = driver.window_handles[0]
		continueButton = wait.until(EC.presence_of_element_located((By.XPATH,"//a[text()=' Continue ']")))
		if continueButton is None:
			return
		else:
			continueButton.click()
			time.sleep(5)
			newWindow = driver.window_handles[1]
			driver.switch_to_window(newWindow)
			username = wait.until(EC.presence_of_element_located((By.XPATH,"//p[text()='"+input.email+"']")))
			username.click()
			time.sleep(10)
			allowPermissionButton = wait.until(EC.presence_of_element_located((By.XPATH,"//span[text()='ALLOW']")))
			allowPermissionButton.click()
			time.sleep(10)
			driver.switch_to_window(mailWindow)
			import random
			mailSubject  = input.mailSubject+str(random.randint(1000,100000))
			try:
				donePopupButton = wait.until(EC.presence_of_element_located((By.XPATH,"//button[text()='Done']")))
				if donePopupButton is None:
					return
				else:
					donePopupButton.click()
					time.sleep(10)
					self.send_email(input.senderEmail,input.senderPassword,input.email,mailSubject,input.mailText)
			except:
				print "Done button does not appear"
				self.send_email(input.senderEmail,input.senderPassword,input.email,mailSubject,input.mailText)
			"""
			remove driver.refresh() to checkMail without refresh
			"""
			count = 1
			mailPresent = False
			while(count<input.mailMaxWait):
				try:
					autoEmailElements = driver.find_elements_by_xpath("//b[text()='"+mailSubject+"']")
					if len(autoEmailElements) > 0:
						mailPresent = True
						print ("Mail Appear Count : "+str(count))
						break
					else:
						count=count+1
						time.sleep(1)
						driver.refresh()
				except:
					count=count+1
					driver.refresh()
			if mailPresent:
				labelCount = 1
				labelPresent = False
				"""
				comment driver.refresh() in below lines to test label without refresh
				"""
				while(labelCount <input.labelMaxWait):
					try:
						actionableLabel = driver.find_element_by_xpath("//b[text()='"+mailSubject+"']/preceding::div[text()='@actionable']/preceding::span[text()='"+input.senderUsername+"'][1]")
						if actionableLabel is not None:
							labelPresent = True
							print ("Label Appear Count : " + str(labelCount) )
							break
						else:
							labelCount = labelCount+1
							time.sleep(1)
							driver.refresh()
					except:
						labelCount = labelCount+1
						time.sleep(1)
						driver.refresh()
				if labelPresent:
					print("labelAppear")
					resultString = str(count) + "," + str(labelCount)
					self.write_file(resultString)
				else:
					resultString = str(count) + "," + str(-1)
					self.write_file(resultString)
					import traceback
					traceback.print_exc()
					raise Exception("Label Failed to appear after waiting for 10 seconds")

	def tearDown(self):
		self.driver.close()


if __name__ == "__main__":
	unittest.main()
		